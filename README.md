PiGamePad
===========================

Use a gamepad on a raspberry pi. This package is designed to be simple to use, but not super powerful.

All pads should work, only the SNES pad has button definitions so far.