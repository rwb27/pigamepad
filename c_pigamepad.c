//many ideas taken from https://gist.github.com/zed/3471699

#define Py_LIMITED_API
#define PY_SSIZE_T_CLEAN
#include "Python.h"

#include <fcntl.h>
#include <linux/joystick.h>


#ifdef __cplusplus
extern "C" {
#endif

static void* python_set_value(void *python_callback,struct js_event data)
{
    int error_flag = 0;
    PyObject* tmp;
    int value=7;
    PyGILState_STATE state = PyGILState_Ensure();
    // call python callback
    tmp = PyObject_CallFunction((PyObject*)python_callback, "kiHH", data.time,data.value,data.type,data.number);
    //format is (unsigned long,int,uint8,uint8) see https://docs.python.org/3/c-api/arg.html
    if (tmp == NULL)
    {
        PyErr_PrintEx(0);
        error_flag = 1; // error occured
    }
    Py_XDECREF(tmp);
    PyGILState_Release(state);
    return error_flag ? NULL : python_callback;
}

static PyObject* monitor_pad(PyObject *self_unused, PyObject *args)
{
    PyObject* ret;
    PyObject* python_callback;
    int error_flag = 0;


    //NEW
    int fd;
    struct js_event js;
    char* controller;
    fd_set set;
    struct timeval timeout;
    int rv;

    PyEval_InitThreads();
    //Parse inputs from python
    if (!PyArg_ParseTuple(args, "Os:monitor_pad",&python_callback, &controller))
    {
        return NULL; // propagate exception
    }
    Py_INCREF(python_callback); // increment reference counter to hold on

    if ((fd = open(controller, O_RDONLY)) < 0)
    {
        printf("Cannot find %s",controller);
        error_flag=1;
    }
    else
    {
        int n;
        n=0;
        
        Py_BEGIN_ALLOW_THREADS
        while (1)
        {
            if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event))
            {
                printf("Pad Monitor Error\n");
                return 1;
            }
            //printf("Event: type %d, time %d, number %d, value %d\n",js.type, js.time, js.number, js.value);
            if (!python_set_value(python_callback,js))
            {
                error_flag = 1;
                break;
            }
            fflush(stdout);
        }
        Py_END_ALLOW_THREADS
    }
    // check for errors
    if (!error_flag)
    {  // no error
        ret = Py_None;
        Py_INCREF(ret);
    }
    else
    {
        ret = NULL;
        PyErr_SetString(PyExc_RuntimeError, "callback failed");
    }
    

    Py_DECREF(python_callback); //Decrement reference counter to release
    return ret;
}

static PyMethodDef module_functions[] = {{ "monitor_pad", monitor_pad,
                METH_VARARGS, "func docstring" },{ NULL, NULL, 0, NULL }};

// http://python3porting.com/cextensions.html
#if PY_MAJOR_VERSION >= 3
    #define MOD_ERROR_VAL NULL
    #define MOD_SUCCESS_VAL(val) val
    #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
    #define MOD_DEF(ob, name, doc, methods) \
            static struct PyModuleDef moduledef = { \
            PyModuleDef_HEAD_INIT, name, doc, -1, methods, }; \
            ob = PyModule_Create(&moduledef);
#else
    #define MOD_ERROR_VAL
    #define MOD_SUCCESS_VAL(val)
    #define MOD_INIT(name) void init##name(void)
    #define MOD_DEF(ob, name, doc, methods) \
            ob = Py_InitModule3(name, methods, doc);
#endif

MOD_INIT(c_pigamepad)
{
    PyObject *m = NULL;
    MOD_DEF(m, "c_pigamepad", "module docstring",  module_functions)
    if (m == NULL)
        return MOD_ERROR_VAL;
    return MOD_SUCCESS_VAL(m);
}

#ifdef __cplusplus
}
#endif